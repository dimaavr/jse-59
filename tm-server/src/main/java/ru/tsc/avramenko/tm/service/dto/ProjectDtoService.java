package ru.tsc.avramenko.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.avramenko.tm.api.repository.dto.IProjectDtoRepository;
import ru.tsc.avramenko.tm.api.service.dto.IProjectDtoService;
import ru.tsc.avramenko.tm.dto.ProjectDTO;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.exception.empty.*;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.exception.system.IndexIncorrectException;
import ru.tsc.avramenko.tm.repository.dto.ProjectDtoRepository;
import ru.tsc.avramenko.tm.service.AbstractService;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectDtoService extends AbstractService implements IProjectDtoService {

    @NotNull
    @Autowired
    private IProjectDtoRepository repository;

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        repository.add(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        repository.add(project);
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.findByName(userId, name);
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index > repository.findAllById(userId).size() - 1) throw new IndexIncorrectException();
        return repository.findByIndex(userId, index);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final ProjectDTO project = Optional.ofNullable(repository.findByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        repository.update(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final ProjectDTO project = Optional.ofNullable(repository.findById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        repository.update(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO startById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ProjectDTO project = Optional.ofNullable(repository.findById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        repository.update(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO startByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectDTO project = Optional.ofNullable(repository.findByName(userId, name))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        repository.update(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ProjectDTO project = Optional.ofNullable(repository.findByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        repository.update(project);
        return project;
    }

    @Nullable
    @SneakyThrows
    @Transactional
    public ProjectDTO finishById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ProjectDTO project = Optional.ofNullable(repository.findById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        repository.update(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO finishByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectDTO project = Optional.ofNullable(repository.findByName(userId, name))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        repository.update(project);
        repository.getTransaction().commit();
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO finishByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ProjectDTO project = Optional.ofNullable(repository.findByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        repository.update(project);
        repository.getTransaction().commit();
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final ProjectDTO project = Optional.ofNullable(repository.findById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        repository.update(project);
        repository.getTransaction().commit();
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO changeStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final ProjectDTO project = Optional.ofNullable(repository.findByName(userId, name))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        repository.update(project);
        repository.getTransaction().commit();
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final ProjectDTO project = Optional.ofNullable(repository.findByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        repository.update(project);
        return project;
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return repository.findAllById(userId);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll() {
        return repository.findAll();
    }

    @Override
    @Transactional
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        repository.clear(userId);
    }

    @Nullable
    @Override
    public ProjectDTO findById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    @Override
    @Transactional
    public void clear() {
        repository.clear();
    }

    @Override
    @Transactional
    public void addAll(@Nullable List<ProjectDTO> projects) {
        if (projects == null) return;
        for (@NotNull final ProjectDTO project : projects) repository.add(project);
    }

}