package ru.tsc.avramenko.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.avramenko.tm.api.repository.dto.IProjectDtoRepository;
import ru.tsc.avramenko.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.avramenko.tm.api.service.dto.IProjectTaskDtoService;
import ru.tsc.avramenko.tm.dto.TaskDTO;
import ru.tsc.avramenko.tm.exception.empty.EmptyIdException;
import ru.tsc.avramenko.tm.exception.empty.EmptyIndexException;
import ru.tsc.avramenko.tm.exception.empty.EmptyNameException;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.repository.dto.ProjectDtoRepository;
import ru.tsc.avramenko.tm.repository.dto.TaskDtoRepository;
import ru.tsc.avramenko.tm.service.AbstractService;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectTaskDtoService extends AbstractService implements IProjectTaskDtoService {

    @NotNull
    @Autowired
    private IProjectDtoRepository projectDtoRepository;

    @NotNull
    @Autowired
    private ITaskDtoRepository taskDtoRepository;

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> findTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        return taskDtoRepository.findAllTaskByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO bindTaskById(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        @Nullable final TaskDTO task = taskDtoRepository.findById(userId, taskId);
        task.setProjectId(projectId);
        taskDtoRepository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO unbindTaskById(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        @Nullable final TaskDTO task = taskDtoRepository.findById(userId, taskId);
        task.setProjectId(null);
        taskDtoRepository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
            taskDtoRepository.unbindAllTaskByProjectId(userId, projectId);
            projectDtoRepository.removeById(userId, projectId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final String projectId = Optional.ofNullable(projectDtoRepository.findByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new)
                .getId();
        taskDtoRepository.unbindAllTaskByProjectId(userId, projectId);
        projectDtoRepository.removeByIndex(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final String projectId = Optional.ofNullable(projectDtoRepository.findByName(userId, name))
                .orElseThrow(ProjectNotFoundException::new)
                .getId();
        taskDtoRepository.unbindAllTaskByProjectId(userId, projectId);
        projectDtoRepository.removeByName(userId, name);
    }

}