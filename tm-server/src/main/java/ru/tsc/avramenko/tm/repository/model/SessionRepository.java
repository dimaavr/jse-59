package ru.tsc.avramenko.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import ru.tsc.avramenko.tm.api.repository.model.ISessionRepository;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import ru.tsc.avramenko.tm.model.Session;

import java.util.List;

@org.springframework.stereotype.Repository
@Scope("prototype")
public final class SessionRepository extends Repository<Session> implements ISessionRepository {

    @NotNull
    public List<Session> findAll() {
        return entityManager
                .createQuery("SELECT e FROM Session e", Session.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    public void remove(Session session) {
        Session reference = entityManager.getReference(Session.class, session.getId());
        entityManager.remove(reference);
    }

    @Override
    public Session findById(@Nullable final String id) {
        return entityManager.find(Session.class, id);
    }

    @Override
    public void clear() {
        entityManager
                .createQuery("DELETE FROM Session e")
                .executeUpdate();
    }

    public void removeById(@Nullable final String id) {
        SessionDTO reference = entityManager.getReference(SessionDTO.class, id);
        entityManager.remove(reference);
    }

}