package ru.tsc.avramenko.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import ru.tsc.avramenko.tm.api.repository.model.IUserRepository;
import ru.tsc.avramenko.tm.model.User;

import java.util.List;

@org.springframework.stereotype.Repository
@Scope("prototype")
public final class UserRepository extends Repository<User> implements IUserRepository {

    @NotNull
    public List<User> findAll() {
        return entityManager
                .createQuery("SELECT e FROM User e", User.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    public User findById(@Nullable final String id) {
        return entityManager.find(User.class, id);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM User e")
                .executeUpdate();
    }

    public void removeById(@Nullable final String id) {
        User reference = entityManager.getReference(User.class, id);
        entityManager.remove(reference);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        List<User> list = entityManager
                .createQuery("SELECT e FROM User e WHERE e.login = :login", User.class)
                .setParameter("login", login)
                .setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        List<User> list = entityManager
                .createQuery("SELECT e FROM User e WHERE e.email = :email", User.class)
                .setParameter("email", email)
                .setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Override
    public void removeUserByLogin(@Nullable final String login) {
        entityManager
                .createQuery("DELETE FROM User e WHERE e.login = :login")
                .setParameter("login", login)
                .executeUpdate();
    }

}