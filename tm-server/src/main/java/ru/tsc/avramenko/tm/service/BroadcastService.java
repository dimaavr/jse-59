package ru.tsc.avramenko.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.tsc.avramenko.tm.api.service.IBroadcastService;
import ru.tsc.avramenko.tm.component.JmsMessageComponent;
import ru.tsc.avramenko.tm.dto.AbstractWrapper;
import ru.tsc.avramenko.tm.enumerated.EntityOperationType;
import ru.tsc.avramenko.tm.util.SystemUtil;

import javax.jms.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class BroadcastService implements IBroadcastService {

    @NotNull
    private final ExecutorService executor = Executors.newFixedThreadPool(3);

    @NotNull
    @SneakyThrows
    private AbstractWrapper getDataForMessage(
            @NotNull final EntityOperationType operationType,
            @NotNull final Object entity
    ) {
        @NotNull AbstractWrapper abstractWrapper = new AbstractWrapper();
        abstractWrapper.setDate(SystemUtil.getCurrentDateTime());
        abstractWrapper.setClassName(entity.getClass().getSimpleName());
        abstractWrapper.setOperation(operationType);
        abstractWrapper.setEntity(entity);
        return abstractWrapper;
    }

    @Override
    public void sendJmsMessageAsync(
            @NotNull final Object entity,
            @NotNull final EntityOperationType operationType
    ) {
        submit(() -> {
            try {
                sendJmsMessageSync(entity, operationType);
            } catch (JMSException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    @SneakyThrows
    public void sendJmsMessageSync(
            @NotNull final Object entity,
            @NotNull final EntityOperationType operationType
    ) throws JMSException {
        @NotNull final Connection connection = JmsMessageComponent.getInstance().getConnection();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final AbstractWrapper abstractWrapper = getDataForMessage(operationType, entity);
        @NotNull final Destination destination = session.createTopic(abstractWrapper.getClassName());
        @NotNull MessageProducer messageProducer = session.createProducer(destination);
        @NotNull final TextMessage textMessage = session.createTextMessage(
                new ObjectMapper().writeValueAsString(abstractWrapper)
        );
        messageProducer.send(textMessage);
        messageProducer.close();
        session.close();
    }

    @Override
    public void submit(@NotNull Runnable runnable) {
        executor.submit(runnable);
    }

    @Override
    public void shutdown() {
        executor.shutdown();
    }

}