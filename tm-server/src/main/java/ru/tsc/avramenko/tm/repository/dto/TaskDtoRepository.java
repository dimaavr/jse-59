package ru.tsc.avramenko.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.avramenko.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.avramenko.tm.dto.TaskDTO;

import java.util.List;

@Repository
@Scope("prototype")
public final class TaskDtoRepository extends DtoRepository<TaskDTO> implements ITaskDtoRepository {

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        return entityManager
                .createQuery("SELECT e FROM TaskDTO e", TaskDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    public List<TaskDTO> findAllTaskByProjectId(String userId, String projectId) {
        return entityManager
                .createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :user_id AND e.projectId = :project_id", TaskDTO.class)
                .setParameter("user_id", userId)
                .setParameter("project_id", projectId)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDTO findByName(@NotNull final String userId, @Nullable final String name) {
        List<TaskDTO> list = entityManager
                .createQuery("SELECT e FROM TaskDTO e WHERE e.name = :name AND e.userId = :user_id", TaskDTO.class)
                .setParameter("name", name)
                .setParameter("user_id", userId)
                .setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Nullable
    @Override
    public TaskDTO findByIndex(@NotNull final String userId, final int index) {
        List<TaskDTO> list = entityManager
                .createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :user_id", TaskDTO.class)
                .setParameter("user_id", userId)
                .setFirstResult(index).setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Override
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        entityManager
                .createQuery("DELETE FROM TaskDTO e WHERE e.name = :name AND e.userId = :user_id")
                .setParameter("user_id", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) {
        entityManager
                .createQuery("DELETE FROM TaskDTO e WHERE e.userId = :user_id")
                .setParameter("user_id", userId)
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM TaskDTO e")
                .executeUpdate();
    }

    @Override
    public void clear(String userId) {
        entityManager
                .createQuery("DELETE FROM TaskDTO e WHERE e.userId = :user_id")
                .setParameter("user_id", userId)
                .executeUpdate();
    }

    @Override
    public TaskDTO findById(String userId, String id) {
        List<TaskDTO> list = entityManager
                .createQuery("SELECT e FROM TaskDTO e WHERE e.id = :id AND e.userId = :user_id", TaskDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("id", id)
                .setParameter("user_id", userId)
                .setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Override
    public void removeById(String userId, String id) {
        entityManager
                .createQuery("DELETE FROM TaskDTO e WHERE e.userId = :user_id AND e.id = :id")
                .setParameter("user_id", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public List<TaskDTO> findAllById(String userId) {
        List<TaskDTO> list = entityManager
                .createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :user_id", TaskDTO.class)
                .setParameter("user_id", userId)
                .getResultList();
        if (list.isEmpty())
            return null;
        else
            return list;
    }

    @Override
    public void unbindAllTaskByProjectId(String userId, String projectId) {
        entityManager
                .createQuery("UPDATE TaskDTO e SET e.projectId = NULL WHERE e.userId = :user_id AND e.projectId = :project_id")
                .setParameter("user_id", userId)
                .setParameter("project_id", projectId)
                .executeUpdate();
    }

}