package ru.tsc.avramenko.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.avramenko.tm.api.repository.model.ISessionRepository;
import ru.tsc.avramenko.tm.api.service.IPropertyService;
import ru.tsc.avramenko.tm.api.service.model.ISessionService;
import ru.tsc.avramenko.tm.api.service.model.IUserService;
import ru.tsc.avramenko.tm.exception.empty.EmptyIdException;
import ru.tsc.avramenko.tm.model.Session;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.service.AbstractService;
import ru.tsc.avramenko.tm.util.HashUtil;

import java.util.List;

@Service
public class SessionService extends AbstractService implements ISessionService {

    @Autowired
    private IUserService userService;

    @Autowired
    private IPropertyService propertyService;

    @Autowired
    private ISessionRepository sessionRepository;

    @Override
    @SneakyThrows
    public boolean checkDataAccess(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) return false;
        if (user.getLocked()) return false;
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) return false;
        return hash.equals(user.getPasswordHash());
    }

    @Override
    @Nullable
    @SneakyThrows
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.sign(propertyService, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void close(@NotNull Session session) {
        validate(session);
        sessionRepository.remove(session);
    }

    @Override
    @SneakyThrows
    @Transactional
    public Session open(@Nullable String login, @Nullable String password) {
        boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        final Session session = new Session();
        session.setUser(user);
        session.setTimestamp(System.currentTimeMillis());
        @Nullable final Session resultSession = sign(session);
        sessionRepository.add(resultSession);
        return resultSession;
    }

    @Override
    @SneakyThrows
    public void validate(@NotNull Session session, @Nullable Role role) throws AccessDeniedException {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final User user = session.getUser();
        @Nullable final String userId = user.getId();
        @Nullable final User userExists = userService.findById(userId);
        if (userExists == null) throw new AccessDeniedException();
        if (userExists.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(userExists.getRole())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable Session session) throws AccessDeniedException {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUser().getId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (sessionRepository.findById(session.getId()) == null) throw new AccessDeniedException();
    }

    @Nullable
    @Override
    public List<Session> findAll() {
        return sessionRepository.findAll();
    }

    @Nullable
    @Override
    public Session findById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return sessionRepository.findById(id);
    }

}