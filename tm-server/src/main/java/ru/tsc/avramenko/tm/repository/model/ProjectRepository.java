package ru.tsc.avramenko.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.avramenko.tm.api.repository.model.IProjectRepository;
import ru.tsc.avramenko.tm.model.Project;
import java.util.List;

@org.springframework.stereotype.Repository
@Scope("prototype")
public final class ProjectRepository extends Repository<Project> implements IProjectRepository {

    public Project findById(@Nullable final String id) {
        return entityManager.find(Project.class, id);
    }

    public void removeById(@Nullable final String id) {
        Project reference = entityManager.getReference(Project.class, id);
        entityManager.remove(reference);
    }

    @NotNull
    public List<Project> findAll() {
        return entityManager
                .createQuery("SELECT e FROM Project e", Project.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public Project findByName(@NotNull final String userId, @Nullable final String name) {
        List<Project> list = entityManager
                .createQuery("SELECT e FROM Project e WHERE e.name = :name AND e.user.id = :user_id", Project.class)
                .setParameter("name", name)
                .setParameter("user_id", userId)
                .setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Nullable
    @Override
    public Project findByIndex(@NotNull final String userId, final int index) {
        List<Project> list = entityManager
                .createQuery("SELECT e FROM Project e WHERE e.user.id = :user_id", Project.class)
                .setParameter("user_id", userId)
                .setFirstResult(index).setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Override
    @Transactional
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        entityManager
                .createQuery("DELETE FROM Project e WHERE e.name = :name AND e.user.id = :user_id")
                .setParameter("user_id", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) {
        entityManager
                .createQuery("DELETE FROM Project e WHERE e.user.id = :user_id")
                .setParameter("user_id", userId)
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM Project e")
                .executeUpdate();
    }

    @Override
    public void clear(final String userId) {
        entityManager
                .createQuery("DELETE FROM Project e WHERE e.user.id = :user_id")
                .setParameter("user_id", userId)
                .executeUpdate();
    }

    @Override
    public Project findById(final String userId, final String id) {
        List<Project> list = entityManager
                .createQuery("SELECT e FROM Project e WHERE e.id = :id AND e.user.id = :user_id", Project.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("id", id)
                .setParameter("user_id", userId)
                .setMaxResults(1).getResultList();
        if (list.isEmpty())
            return null;
        else
            return list.get(0);
    }

    @Override
    @Transactional
    public void removeById(final String userId, final String id) {
        entityManager
                .createQuery("DELETE FROM Project e WHERE e.id = :id AND e.user.id = :user_id")
                .setParameter("user_id", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public List<Project> findAllById(final String userId) {
        List<Project> list = entityManager
                .createQuery("SELECT e FROM Project e WHERE e.user.id = :user_id", Project.class)
                .setParameter("user_id", userId)
                .getResultList();
        if (list.isEmpty())
            return null;
        else
            return list;
    }

}