package ru.tsc.avramenko.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import ru.tsc.avramenko.tm.dto.UserDTO;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IAdminUserEndpoint {

    @WebMethod
    UserDTO lockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "login", partName = "login") @NotNull String login
    );

    @WebMethod
    void removeUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "login", partName = "login") @NotNull String login
    );

    @WebMethod
    void removeUserById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    );

    @WebMethod
    UserDTO unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "login", partName = "login") @NotNull String login
    );

    UserDTO setUserRole(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "userId", partName = "userId") @NotNull String id,
            @WebParam(name = "role", partName = "role") @NotNull Role role
    );

    @WebMethod
    UserDTO updateUserById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "firstName", partName = "firstName") @NotNull String firstName,
            @WebParam(name = "lastName", partName = "lastName") @NotNull String lastName,
            @WebParam(name = "middleName", partName = "middleName") @NotNull String middleName,
            @WebParam(name = "email", partName = "email") @NotNull String email
    );

    @WebMethod
    UserDTO updateUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "login", partName = "login") @NotNull String login,
            @WebParam(name = "firstName", partName = "firstName") @NotNull String firstName,
            @WebParam(name = "lastName", partName = "lastName") @NotNull String lastName,
            @WebParam(name = "middleName", partName = "middleName") @NotNull String middleName,
            @WebParam(name = "email", partName = "email") @NotNull String email
    );

    @WebMethod
    void updateUserPassword(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "password", partName = "password") @NotNull String password
    );

    @Nullable
    @WebMethod
    UserDTO findUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "login", partName = "login") @NotNull String login
    );

    @Nullable
    @WebMethod
    UserDTO findUserById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "idn", partName = "id") @NotNull String id
    );

    @Nullable
    @WebMethod
    void clearUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    );

    @Nullable
    @WebMethod
    List<UserDTO> findAllUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    );

}