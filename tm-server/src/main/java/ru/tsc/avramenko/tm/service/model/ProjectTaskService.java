package ru.tsc.avramenko.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.avramenko.tm.api.repository.model.IProjectRepository;
import ru.tsc.avramenko.tm.api.repository.model.ITaskRepository;
import ru.tsc.avramenko.tm.api.service.model.IProjectTaskService;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.exception.empty.EmptyIdException;
import ru.tsc.avramenko.tm.exception.empty.EmptyIndexException;
import ru.tsc.avramenko.tm.exception.empty.EmptyNameException;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.service.AbstractService;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectTaskService extends AbstractService implements IProjectTaskService {

    @Autowired
    private ITaskRepository taskRepository;

    @Autowired
    private IProjectRepository projectRepository;

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public List<Task> findTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        return taskRepository.findAllTaskByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task bindTaskById(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        @Nullable final Project project = projectRepository.findById(userId, projectId);
        @Nullable final Task task = taskRepository.findById(userId, taskId);
        task.setProject(project);
        taskRepository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task unbindTaskById(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        @Nullable final Task task = taskRepository.findById(userId, taskId);
        task.setProject(null);
        taskRepository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
            taskRepository.unbindAllTaskByProjectId(userId, projectId);
            projectRepository.removeById(userId, projectId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final String projectId = Optional.ofNullable(projectRepository.findByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new)
                .getId();
        taskRepository.unbindAllTaskByProjectId(userId, projectId);
        projectRepository.removeByIndex(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final String projectId = Optional.ofNullable(projectRepository.findByName(userId, name))
                .orElseThrow(ProjectNotFoundException::new)
                .getId();
        taskRepository.unbindAllTaskByProjectId(userId, projectId);
        projectRepository.removeByName(userId, name);
    }

}