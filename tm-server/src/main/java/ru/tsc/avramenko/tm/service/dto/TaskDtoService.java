package ru.tsc.avramenko.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.avramenko.tm.api.repository.dto.ISessionDtoRepository;
import ru.tsc.avramenko.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.avramenko.tm.api.service.dto.ITaskDtoService;
import ru.tsc.avramenko.tm.dto.TaskDTO;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.exception.empty.*;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.repository.dto.TaskDtoRepository;
import ru.tsc.avramenko.tm.service.AbstractService;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TaskDtoService extends AbstractService implements ITaskDtoService {

    @NotNull
    @Autowired
    private ITaskDtoRepository taskDtoRepository;

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyIdException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        taskDtoRepository.add(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        taskDtoRepository.add(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskDtoRepository.findByName(userId, name);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskDtoRepository.findByIndex(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        taskDtoRepository.removeByName(userId, name);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        taskDtoRepository.removeByIndex(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        taskDtoRepository.update(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findById(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        taskDtoRepository.update(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findById(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status);
        taskDtoRepository.update(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO changeStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findByName(userId, name))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status);
        taskDtoRepository.update(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status);
        taskDtoRepository.update(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO startById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findById(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        taskDtoRepository.update(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO startByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findByName(userId, name))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        taskDtoRepository.update(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        taskDtoRepository.update(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO finishById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findById(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        taskDtoRepository.update(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO finishByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findByName(userId, name))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        taskDtoRepository.update(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO finishByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        taskDtoRepository.update(task);
        return task;
    }

    @Override
    @Nullable
    public TaskDTO findById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskDtoRepository.findById(userId, id);
    }

    @Override
    @Transactional
    public @Nullable void removeById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskDtoRepository.removeById(userId, id);
    }

    @Override
    public @Nullable List<TaskDTO> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return taskDtoRepository.findAllById(userId);
    }

    @Override
    public @Nullable List<TaskDTO> findAll() {
        return taskDtoRepository.findAll();
    }

    @Override
    @Transactional
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        taskDtoRepository.clear(userId);
    }

    @Override
    @Transactional
    public void clear() {
        taskDtoRepository.clear();
    }

    @Override
    @Transactional
    public void addAll(@Nullable List<TaskDTO> tasks) {
        if (tasks == null) return;
        for (@NotNull final TaskDTO task : tasks) taskDtoRepository.add(task);
    }

}