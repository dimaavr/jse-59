package ru.tsc.avramenko.tm.repository.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.model.AbstractEntity;

import java.util.Collection;

public abstract class Repository<E extends AbstractEntity> extends ru.tsc.avramenko.tm.repository.Repository {

    public void add(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.persist(entity);
    }

    public void update(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.merge(entity);
    }

    public void addAll(@Nullable final Collection<E> collection) {
        if (collection == null) return;
        for (E item : collection) {
            add(item);
        }
    }

}