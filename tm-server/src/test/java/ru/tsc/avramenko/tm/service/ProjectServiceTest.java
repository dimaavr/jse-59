package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.avramenko.tm.api.service.model.IProjectService;
import ru.tsc.avramenko.tm.api.service.model.IProjectTaskService;
import ru.tsc.avramenko.tm.api.service.model.ISessionService;
import ru.tsc.avramenko.tm.api.service.model.IUserService;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.configuration.ServerConfiguration;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.exception.empty.EmptyIdException;
import ru.tsc.avramenko.tm.exception.empty.EmptyNameException;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.model.Session;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.service.model.ProjectService;
import ru.tsc.avramenko.tm.service.model.ProjectTaskService;
import ru.tsc.avramenko.tm.service.model.SessionService;
import ru.tsc.avramenko.tm.service.model.UserService;

import java.util.List;

public class ProjectServiceTest {

    @Nullable
    private static IProjectService projectService;

    @Nullable
    private static IProjectTaskService projectTaskService;

    @Nullable
    private static ISessionService sessionService;

    @Nullable
    private static IUserService userService;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private Project project;

    @Nullable
    private static Session session;

    @Nullable
    private static User user;

    @NotNull
    protected static final String TEST_PROJECT_NAME = "TestName";

    @NotNull
    protected static final String TEST_DESCRIPTION_NAME = "TestDescription";

    @NotNull
    protected static final String TEST_USER_ID_INCORRECT = "TestUserIdIncorrect";

    @NotNull
    protected static final String TEST_PROJECT_ID_INCORRECT = "647";

    @BeforeClass
    public static void beforeClass() {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        userService = context.getBean(IUserService.class);
        projectService = context.getBean(IProjectService.class);
        projectTaskService = context.getBean(IProjectTaskService.class);
        sessionService = context.getBean(ISessionService.class);
        user = userService.create("Test", "Test");
    }

    @Before
    public void before() {
        session = sessionService.open("Test", "Test");
        projectService.create(session.getUser().getId(), TEST_PROJECT_NAME, TEST_DESCRIPTION_NAME);
        project = projectService.findByName(session.getUser().getId(), TEST_PROJECT_NAME);
    }

    @After
    public void after() {
        projectService.clear(session.getUser().getId());
        sessionService.close(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        Assert.assertEquals(TEST_PROJECT_NAME, project.getName());
        Assert.assertEquals(TEST_DESCRIPTION_NAME, project.getDescription());

        @Nullable final Project projectById = projectService.findById(session.getUser().getId(), project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project.getId(), projectById.getId());
    }

    @Test
    public void findAll() {
        @NotNull final List<Project> projects = projectService.findAll();
        Assert.assertTrue(projects.size() > 0);
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Project> projects = projectService.findAll(session.getUser().getId());
        Assert.assertTrue(projects.size() > 0);
    }

    @Test
    public void findById() {
        @Nullable final Project project = projectService.findById(session.getUser().getId(), this.project.getId());
        Assert.assertNotNull(project);
    }

    @Test
    public void findByIdIncorrect() {
        @Nullable final Project project = projectService.findById(session.getUser().getId(), TEST_PROJECT_ID_INCORRECT);
        Assert.assertNull(project);
    }

    @Test
    public void findByIdIncorrectUser() {
        @Nullable final Project project = projectService.findById(TEST_USER_ID_INCORRECT, this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    public void findByName() {
        @NotNull final Project project = projectService.findByName(session.getUser().getId(), TEST_PROJECT_NAME);
        Assert.assertNotNull(project);
    }

    @Test
    public void findByNameIncorrect() {
        @Nullable final Project project = projectService.findByName(session.getUser().getId(), TEST_PROJECT_ID_INCORRECT);
        Assert.assertNull(project);
    }

    @Test(expected = EmptyNameException.class)
    public void findByNameNull() {
        @Nullable final Project project = projectService.findByName(session.getUser().getId(), null);
        Assert.assertNull(project);
    }

    @Test
    public void findByNameIncorrectUser() {
        @Nullable final Project project = projectService.findByName(TEST_USER_ID_INCORRECT, this.project.getName());
        Assert.assertNull(project);
    }

    @Test
    public void removeById() {
        projectTaskService.removeProjectById(session.getUser().getId(), project.getId());
        Assert.assertNull(projectService.findById(session.getUser().getId(), project.getId()));
    }

    @Test(expected = EmptyIdException.class)
    public void removeByIdNull() {
        projectTaskService.removeProjectById(session.getUser().getId(), null);
        Assert.assertNotNull(projectService.findById(session.getUser().getId(), project.getId()));
    }

    @Test
    public void removeByIdIncorrect() {
        projectTaskService.removeProjectById(session.getUser().getId(), TEST_PROJECT_ID_INCORRECT);
        Assert.assertNotNull(projectService.findById(session.getUser().getId(), project.getId()));
    }

    @Test
    public void removeByIdIncorrectUser() {
        projectTaskService.removeProjectById(TEST_USER_ID_INCORRECT, this.project.getId());
        Assert.assertNotNull(projectService.findById(session.getUser().getId(), project.getId()));
    }

    @Test
    public void startById() {
        @Nullable final Project project = projectService.startById(session.getUser().getId(), this.project.getId());
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test(expected = EmptyIdException.class)
    public void startByIdNull() {
        @Nullable final Project project = projectService.startById(session.getUser().getId(), null);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void startByIdIncorrect() {
        @Nullable final Project project = projectService.startById(session.getUser().getId(), TEST_PROJECT_ID_INCORRECT);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void startByIdIncorrectUser() {
        @Nullable final Project project = projectService.startById(TEST_USER_ID_INCORRECT, this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    public void startByName() {
        @Nullable final Project project = projectService.startByName(session.getUser().getId(), TEST_PROJECT_NAME);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test(expected = EmptyNameException.class)
    public void startByNameNull() {
        @Nullable final Project project = projectService.startByName(session.getUser().getId(), null);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void startByNameIncorrect() {
        @Nullable final Project project = projectService.startByName(session.getUser().getId(), TEST_PROJECT_ID_INCORRECT);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void startByNameIncorrectUser() {
        @Nullable final Project project = projectService.startByName(TEST_USER_ID_INCORRECT, TEST_PROJECT_NAME);
        Assert.assertNull(project);
    }

    @Test
    public void startByIndex() {
        @Nullable final Project project = projectService.startByIndex(session.getUser().getId(), 0);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test(expected = ProjectNotFoundException.class)
    public void startByIndexIncorrect() {
        @Nullable final Project project = projectService.startByIndex(session.getUser().getId(), 674);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void startByIndexIncorrectUser() {
        @Nullable final Project project = projectService.startByIndex(TEST_USER_ID_INCORRECT, 674);
        Assert.assertNull(project);
    }

    @Test
    public void finishById() {
        @Nullable final Project project = projectService.finishById(session.getUser().getId(), this.project.getId());
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test(expected = EmptyIdException.class)
    public void finishByIdNull() {
        @Nullable final Project project = projectService.finishById(session.getUser().getId(), null);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void finishByIdIncorrect() {
        @Nullable final Project project = projectService.finishById(session.getUser().getId(), TEST_PROJECT_ID_INCORRECT);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void finishByIdIncorrectUser() {
        @Nullable final Project project = projectService.finishById(TEST_USER_ID_INCORRECT, this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    public void finishByName() {
        @Nullable final Project project = projectService.finishByName(session.getUser().getId(), TEST_PROJECT_NAME);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test(expected = EmptyNameException.class)
    public void finishByNameNull() {
        @Nullable final Project project = projectService.finishByName(session.getUser().getId(), null);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void finishByNameIncorrect() {
        @Nullable final Project project = projectService.finishByName(session.getUser().getId(), TEST_PROJECT_ID_INCORRECT);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void finishByNameIncorrectUser() {
        @Nullable final Project project = projectService.finishByName(TEST_USER_ID_INCORRECT, TEST_PROJECT_NAME);
        Assert.assertNull(project);
    }

    @Test
    public void finishByIndex() {
        @Nullable final Project project = projectService.finishByIndex(session.getUser().getId(), 0);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test(expected = ProjectNotFoundException.class)
    public void finishByIndexIncorrect() {
        @Nullable final Project project = projectService.finishByIndex(session.getUser().getId(), 674);
        Assert.assertNull(project);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void finishByIndexIncorrectUser() {
        @Nullable final Project project = projectService.finishByIndex(TEST_USER_ID_INCORRECT, 674);
        Assert.assertNull(project);
    }

    @Test
    public void changeStatusById() {
        @Nullable final Project project = projectService.changeStatusById(session.getUser().getId(), this.project.getId(), Status.NOT_STARTED);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
    }

    @Test
    public void changeStatusByName() {
        @Nullable final Project project = projectService.changeStatusByName(session.getUser().getId(), TEST_PROJECT_NAME, Status.NOT_STARTED);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
    }

    @Test
    public void changeStatusByIndex() {
        @Nullable final Project project = projectService.changeStatusByIndex(session.getUser().getId(), 0, Status.NOT_STARTED);
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
    }

}