package ru.tsc.avramenko.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.ISessionService;
import ru.tsc.avramenko.tm.endpoint.*;
import ru.tsc.avramenko.tm.event.ConsoleEvent;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import java.util.Optional;

@Component
public class BackupLoadListener extends AbstractDataListener {

    @Autowired
    private AdminDataEndpoint adminDataEndpoint;

    @Autowired
    private ISessionService sessionService;

    @NotNull
    public final static String BACKUP_LOAD = "backup-load";

    @NotNull
    @Override
    public String name() {
        return BACKUP_LOAD;
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load backup.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@backupLoadListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @Nullable final SessionDTO session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        adminDataEndpoint.loadBackup(session);
    }

    @Nullable
    public Role[] roles() {
        if (sessionService.getSession() == null) {
            return null;
        }
        else {
            return new Role[]{Role.ADMIN};
        }
    }

}